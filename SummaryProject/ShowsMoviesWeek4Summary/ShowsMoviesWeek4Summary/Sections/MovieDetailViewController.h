//
//  MovieDetailViewController.h
//  ShowsMoviesWeek4Summary
//
//  Created by Daniel García on 29/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Movie;
@interface MovieDetailViewController : UIViewController
- (instancetype)initWithMovie:(Movie *)movie;
@end
