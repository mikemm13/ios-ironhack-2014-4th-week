//
//  TVShow+MediaItem.h
//  ShowsMoviesWeek4Summary
//
//  Created by Daniel García on 27/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "TVShow.h"
#import "MediaItemProtocol.h"

@interface TVShow (MediaItem)<MediaItemProtocol>

@end
