//
//  MoviesProvider.m
//  ShowsMoviesWeek4Summary
//
//  Created by Daniel García on 27/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "MoviesProvider.h"
#import "Movie.h"

@implementation MoviesProvider
- (void)moviesWithSuccessBlock:(RequestManagerSuccess)successBlock errorBlock:(RequestManagerError)errorBlock{
    NSString *path=@"movies/trending.json/df2880909bafa2689135188bf47fe5f9";
    NSDictionary *parameters=@{};
    [self.requestManager GET:path parameters:parameters successBlock:^(id data) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            NSMutableArray *movies=[NSMutableArray array];
            for (NSDictionary *movieDictionary in data) {
                Movie *movie = [MTLJSONAdapter modelOfClass:[Movie class] fromJSONDictionary:movieDictionary error:nil];
                [movies addObject:movie];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                successBlock(movies);
            });
        });
    } errorBlock:^(NSError *error) {
        errorBlock(error);
    }];
}
@end
