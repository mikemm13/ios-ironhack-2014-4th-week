//
//  TVShowsProvider.m
//  ShowsMoviesWeek4Summary
//
//  Created by Daniel García on 27/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "TVShowsProvider.h"
#import "TVShow.h"

@implementation TVShowsProvider
- (void)showsWithSuccessBlock:(RequestManagerSuccess)successBlock errorBlock:(RequestManagerError)errorBlock{
    NSString *path=@"shows/trending.json/df2880909bafa2689135188bf47fe5f9";
    NSDictionary *parameters=@{};
    [self.requestManager GET:path parameters:parameters successBlock:^(id data) {
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
            NSMutableArray *shows=[NSMutableArray array];
            for (NSDictionary *showDictionary in data) {
                TVShow *show = [MTLJSONAdapter modelOfClass:[TVShow class] fromJSONDictionary:showDictionary error:nil];
                [shows addObject:show];
            }
            dispatch_async(dispatch_get_main_queue(), ^{
                successBlock(shows);
            });
        });
    } errorBlock:^(NSError *error) {
        errorBlock(error);
    }];
}
@end
