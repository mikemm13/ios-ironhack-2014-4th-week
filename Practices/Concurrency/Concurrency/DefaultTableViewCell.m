//
//  DefaultTableViewCell.m
//  Concurrency
//
//  Created by Daniel García García on 26/05/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "DefaultTableViewCell.h"

@implementation DefaultTableViewCell

- (void)awakeFromNib
{
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
