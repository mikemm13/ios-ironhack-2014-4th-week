//
//  ImageDownloader.h
//  Concurrency
//
//  Created by Daniel García on 26/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ImageDownloader : NSObject
+ (instancetype)defaultDownloader;
- (void)downloadImageWithURL:(NSURL *)imageURL completion:(void(^)(UIImage *image))completion;
@end
