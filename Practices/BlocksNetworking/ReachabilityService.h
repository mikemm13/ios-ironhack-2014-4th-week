//
//  ReachabilityService.h
//  BlocksNetworking
//
//  Created by Daniel García on 25/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import <Foundation/Foundation.h>

UIKIT_EXTERN NSString * const ReachabilityServiceChangeAvailable;
UIKIT_EXTERN NSString * const ReachabilityServiceChangeNotAvailable;

@interface ReachabilityService : NSObject
- (instancetype)init;
@end
