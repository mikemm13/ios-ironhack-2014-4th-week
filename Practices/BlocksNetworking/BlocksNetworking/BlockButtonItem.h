//
//  BlockButtonItem.h
//  BlocksNetworking
//
//  Created by Daniel García García on 21/05/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import <UIKit/UIKit.h>

typedef void (^barButtonItemBlock)();
@interface BlockButtonItem : UIBarButtonItem
- (instancetype)initWithTitle:(NSString *)title block:(barButtonItemBlock)block;
@end
