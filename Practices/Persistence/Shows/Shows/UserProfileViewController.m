//
//  UserProfileViewController.m
//  Shows
//
//  Created by Daniel García on 24/06/14.
//  Copyright (c) 2014 Produkt. All rights reserved.
//

#import "UserProfileViewController.h"
#import "LoginViewController.h"
#import "UserEntity.h"

@interface UserProfileViewController ()
@property (weak, nonatomic) IBOutlet UILabel *userNameLabel;
- (IBAction)logoutButtonAction:(id)sender;
@end

@implementation UserProfileViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        self.title = @"Profile";
        [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(userDidLogin:) name:UserDidLogNotification object:nil];
    }
    return self;
}
- (void)userDidLogin:(NSNotification *)notification{
    [self updateUserInfoWithUser:[self loggedUser]];
}
- (UserEntity *)loggedUser{
    UserEntity *loggedUser;
    NSFetchRequest *request=[[NSFetchRequest alloc] initWithEntityName:NSStringFromClass([UserEntity class])];
    request.predicate=[NSPredicate predicateWithFormat:@"1=1"];
    NSArray *users=[self.managedObjectContext executeFetchRequest:request error:nil];
    if (users.count) {
        loggedUser=[users firstObject];
    }
    return loggedUser;
}

- (void)updateUserInfoWithUser:(UserEntity *)user{
    self.userNameLabel.text = user.userName;
}
- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
}
- (void)viewWillAppear:(BOOL)animated{
    [super viewWillAppear:animated];
    [self updateUserInfoWithUser:[self loggedUser]];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)logoutButtonAction:(id)sender {
    [self logoutUser];
}
- (void)logoutUser{
    [self deleteUsersCache];
    [self deleteLoggedUser];
    
    LoginViewController *loginViewController = [[LoginViewController alloc]initWithManagedObjectContext:self.managedObjectContext];
    [self.tabBarController presentViewController:loginViewController animated:YES completion:^{
        [self.tabBarController setSelectedIndex:0];
    }];
}
- (NSString *)savesFilePath{
    UserEntity *loggedUser = [self loggedUser];
    NSString *savesFilePath;
    if (loggedUser) {
        NSString *cachesPath = [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) firstObject];
        savesFilePath = [cachesPath stringByAppendingPathComponent:[NSString stringWithFormat:@"%@_saves.plist",loggedUser.userName]];
    }
    return savesFilePath;
}
- (void)deleteUsersCache{
    NSFileManager *fileManger = [NSFileManager defaultManager];
    NSString *savesFilePath = [self savesFilePath];
    if ([fileManger fileExistsAtPath:savesFilePath]) {
        [self logSavesFileAttribuesAtPath:savesFilePath];
        NSError *error;
        [fileManger removeItemAtPath:savesFilePath error:&error];
        if (error) {
            NSLog(@"%@",error);
        }
    }
}
- (void)logSavesFileAttribuesAtPath:(NSString *)savesFilePath{
    NSFileManager *fileManger = [NSFileManager defaultManager];
    NSError *error;
    NSDictionary *attributes = [fileManger attributesOfItemAtPath:savesFilePath error:&error];
    if (error) {
        NSLog(@"%@",error);
    }
    NSLog(@"%@",attributes);
}
- (void)deleteLoggedUser{
    [self.managedObjectContext deleteObject:[self loggedUser]];
    NSError *error;
    [self.managedObjectContext save:&error];
    if (error) {
        NSLog(@"%@",error);
    }
}
@end
