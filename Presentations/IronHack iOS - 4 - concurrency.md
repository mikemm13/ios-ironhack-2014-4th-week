#Concurrency

## IronHack iOS Bootcamp 
##![100%,inline](assets/ironhacklogo.png)
### Week 4 - Day 4


####Daniel García - Produkt

---

#Thread / Queue

---

#Concurrent / Parallel

---

#Grand Central Dispatch (GCD)
#![inline,140%](assets/gcd.png)
##Low level API for concurrency management

---

#GCD
##One of the biggest advances of GCD is that makes our application agnostic of the hardware architecture

---

#GCD
##Code agnostic of the number of available processing cores
##Executes operations on queues, not threads

---

#GCD dispatch_async

```objectivec

/// Here executes on one queue

dispatch_queue_t dispatch_queue=dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
dispatch_async(dispatch_queue, ^{

	/// Here executes on other queue 
        
});

```
We can execute operations on another queue so two operations execute in parallel, or because we don't want the first operation blocks a critic queue. ex: main_queue (Main Thread)

`dispatch_async` returns immediately

---

#GCD

###Each thread executes only one queue at a time. Although, one queue may be executing multiple operations on different thread simultaneously (concurrent).

---

#GCD (main_queue)

##main_queue is a special case of dispatch queue that *always* execute on one single thread

---

#GCD (main_queue)
##It is the default queue. *ALL* UI related is executed on this queue (UIKit).

---

#PRACTICE

---

#GCD - dispatch_sync 

```objectivec

/// Here executes on one queue

dispatch_queue_t dispatch_queue=dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0);
dispatch_sync(dispatch_queue, ^{

	/// Here executes on other queue 
        
});

/// Continues when dispatch_sync finishes

```

We may want to execute something on another thread, but hold the current execution until that operation finishes.

`dispatch_sync` returns when operation finishes

---

#GCD - dispatch_once

```objectivec
+ (instancetype)sharedInstance{
    static dispatch_once_t onceToken;
    static SingletonClass *instance;
    dispatch_once(&onceToken, ^{
        instance = [[SingletonClass alloc]init];
    });
    return instance;
}
```
`dispatch_once` allow us to execute code one single time during the execution of our application. 
This is the most usual and recommended way to create a singleton, so this is the only way we can assure that the singleton creation is *thread safe*

---

#GCD - dispatch\_queue\_create

```objectivec
dispatch_queue_t dispatch_queue = dispatch_queue_create(
            "com.myawesomeapp.process.processdata", 
            DISPATCH_QUEUE_CONCURRENT);
```

`dispatch_queue_create` creates a new queue that can be retained and reused. 
`DISPATCH_QUEUE_CONCURRENT` makes the queue to be able to handle multiples operations concurrently

---

#GCD - dispatch\_queue\_create

```objectivec
dispatch_queue_t dispatch_queue = dispatch_queue_create(
            "com.myawesomeapp.process.processdata", 
            DISPATCH_QUEUE_SERIAL);
```

`dispatch_queue_create` creates a new queue that can be retained and reused
`DISPATCH_QUEUE_SERIAL` makes the queue limited to one operation at a time

---

#PRACTICE

---

#NSOperationQueue
##High level API for concurrency management (runs on top GCD)

---

#NSOperationQueue
###Allow us to make concurrency at a high level of abstraction, making easier some tasks that would be way more complex using just GCD

---

#NSOperation

##Abstract Class to encapsulate code and data related to a single task

---

#NSBlockOperation

```objectivec
NSBlockOperation *operation=[NSBlockOperation blockOperationWithBlock:^{
        // Task code
}];
```

`NSBlockOperation` is an `NSOperation` subclass for creating operations with objective-c code blocks

---

#NSBlockOperation
```objectivec
NSBlockOperation *operation=[NSBlockOperation blockOperationWithBlock:^{
        // First block code
}];
[operation addExecutionBlock:^{
        // Second block code
}]; 
```
`NSBlockOperation` allow us to add more than one block of code and execute them all at a time. 
The operation will be considered finished when *all* of blocks are done executing

---

#NSOperationQueue

###Manages operations execution an dependencies.

---

#NSOperationQueue (addOperation)

```objectivec
NSOperation *operation;
NSOperationQueue *backgroundOperationQueue=[[NSOperationQueue alloc]init];
backgroundOperationQueue.maxConcurrentOperationCount=1;
[backgroundOperationQueue addOperation:operation];
```
---

#Operation Dependencies

##Sometimes you need an operation to start *after* other operation completes

---

#Operation Dependencies

```objectivec
NSOperation *firstOperation;
NSOperation *secondOperation;
NSOperation *thirdOperation;

NSOperationQueue *backgroundOperationQueue=[[NSOperationQueue alloc]init];
backgroundOperationQueue.maxConcurrentOperationCount=5;

[thirdOperation addDependency:firstOperation];

[backgroundOperationQueue addOperation:firstOperation];
[backgroundOperationQueue addOperation:secondOperation];
[backgroundOperationQueue addOperation:thirdOperation];
```

---

#PRACTICE
